# ty_netcore_blog_admin

#### 介绍
ASP.NET Core Api + Vue前后端分离博客，此为前端后台管理

#### 软件架构
vue2.6、vue cli4.5、Ant Design of Vue
  
  
TyNetCoreBlog项目 链接：<https://gitee.com/yuguofu/ty-net-core-blog> 为ASP.NET Core后端Api  

ty_netcore_blog_admin 链接：<https://gitee.com/yuguofu/ty_netcore_blog_admin> 项目为vue前端后台管理  

ty_netcore_blog_home项目为vue前端前台展示  

  
#### 安装教程

1.  visual studio 2019打开TyNetCoreBlog项目编译运行
2.  ty_netcore_blog_admin克隆到本地，npm install安装依赖，npm run serve运行调试，npm run build打包发布，浏览器 <http://localhost:8088/> 查看后台管理
3.  ty_netcore_blog_home克隆到本地，npm install安装依赖，npm run serve运行调试，npm run build打包发布，浏览器 <http://localhost:8087/> 查看前台展示

#### 使用说明

1.  visual studio 2019打开TyNetCoreBlog项目编译运行
2.  ty_netcore_blog_admin克隆到本地，npm install安装依赖，npm run serve运行调试，npm run build打包发布，浏览器 <http://localhost:8088/> 查看后台管理
3.  ty_netcore_blog_home克隆到本地，npm install安装依赖，npm run serve运行调试，npm run build打包发布，浏览器 <http://localhost:8087/> 查看前台展示

#### 预览
![登录](https://images.gitee.com/uploads/images/2020/1201/211305_9a6f288c_5348320.png "屏幕截图.png")  
![文章管理](https://images.gitee.com/uploads/images/2020/1201/211655_bcec1bfa_5348320.png "屏幕截图.png")  
![用户管理](https://images.gitee.com/uploads/images/2020/1201/211733_cdd50c62_5348320.png "屏幕截图.png")  
![写文章](https://images.gitee.com/uploads/images/2020/1201/212028_505fa09e_5348320.png "屏幕截图.png")